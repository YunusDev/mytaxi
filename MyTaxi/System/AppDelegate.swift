//
//  AppDelegate.swift
//  MyTaxi
//
//  Created by mac on 18/12/21.
//

import UIKit
import GoogleMaps
import GooglePlaces
@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        GMSServices.provideAPIKey("AIzaSyA4p9U6LanshNeKDvQcu2itMJQCcwMJ7MU")
        GMSPlacesClient.provideAPIKey("AIzaSyA4p9U6LanshNeKDvQcu2itMJQCcwMJ7MU")
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.makeKeyAndVisible()
        self.window?.backgroundColor = .white
        
        let controller = MainScreen()
        let navigationController = UINavigationController(rootViewController: controller)
        self.window?.rootViewController = navigationController
        return true
    }

}

