//
//  Extensions.swift
//  MyTaxi
//
//  Created by mac on 19/12/21.
//

import Foundation
import UIKit

extension UIColor {
    static let appColor = ApplicationColors()
}

struct ApplicationColors {
    let sidebarBackground = UIColor(named: "sidebarBackground")
    let userBackgroundColor = UIColor(named: "userBackgroundColor")
    let cellViewColor = UIColor(named: "cellViewColor")
    let cellBackgroundColor = UIColor(named: "cellBackgroundColor")
    let fromLabelColor = UIColor(named: "fromLabelColor")
    let fromButtonBack = UIColor(named: "fromButtonBack")
    let highlightColor = UIColor(named: "highlightColor")
    let intoButtonBack = UIColor(named: "intoButtonBack")
    let dividerColor = UIColor(named: "dividerColor")
}

class HighlightedButton: UIButton {

    override var isHighlighted: Bool {
        didSet {
            backgroundColor = isHighlighted ? .appColor.highlightColor : .appColor.fromButtonBack
        }
    }
}

class HighlightedButton2: UIButton {

    override var isHighlighted: Bool {
        didSet {
            backgroundColor = isHighlighted ? .appColor.highlightColor : .appColor.intoButtonBack
        }
    }
}

class HelpButton: UIButton  {
    override var isHighlighted: Bool {
        didSet {
            backgroundColor = isHighlighted ? #colorLiteral(red: 1, green: 0.5763723254, blue: 0, alpha: 0.3148772385) : UIColor(named: "helpBtnColor")
        }
    }
}

class ReplayButton: UIButton  {
    override var isHighlighted: Bool {
        didSet {
            backgroundColor = isHighlighted ? #colorLiteral(red: 0, green: 0.5647058824, blue: 1, alpha: 0.5445459631) : #colorLiteral(red: 0.9254901961, green: 0.9490196078, blue: 0.9921568627, alpha: 1)
        }
    }
}

class CallButton: UIButton  {
    override var isHighlighted: Bool {
        didSet {
            backgroundColor = isHighlighted ? #colorLiteral(red: 0.2784313725, green: 0.6235294118, blue: 0.4156862745, alpha: 0.5089947025) : #colorLiteral(red: 0.9333333333, green: 0.9764705882, blue: 0.9490196078, alpha: 1)
        }
    }
}

class FindButton: UIButton  {
    override var isHighlighted: Bool {
        didSet {
            backgroundColor = isHighlighted ? #colorLiteral(red: 0.9994240403, green: 0.9855536819, blue: 0, alpha: 0.281612472) : #colorLiteral(red: 0.9994240403, green: 0.9855536819, blue: 0, alpha: 1)
        }
    }
}

extension UIButton {
    func adjustImageAndTitleOffsetsForButton (button: UIButton) {

        
        let spacing: CGFloat = 3.0

        let imageSize = button.imageView!.frame.size

        button.titleEdgeInsets = UIEdgeInsets(top: 0, left: imageSize.width, bottom: -(imageSize.height + spacing + 10), right: 0)

        let titleSize = button.titleLabel!.frame.size

        button.imageEdgeInsets = UIEdgeInsets(top: -(titleSize.height + spacing), left: 0, bottom: 0, right: -titleSize.width)
    }
    
}
