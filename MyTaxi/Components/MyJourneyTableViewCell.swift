//
//  MyJourneyRowView.swift
//  MyTaxi
//
//  Created by mac on 18/12/21.
//

import SnapKit
import UIKit
 
class MyJourneyTableViewCell: UITableViewCell {
    // MARK: UI Elements
    var belowIphoneVersion8 = false
     var fromPlaceName: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: Components.deviceWidth < 375 ? 10 : 14)
        label.frame.size.width = 200
        label.frame.size.height = 20
        return label
    }()
     var intoplaceName: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: Components.deviceWidth < 375 ? 10 : 14)
        label.frame.size.width = 200
        label.frame.size.height = 20
        return label
    }()
     var intoplaceicon: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "intoIcon")
        imageView.frame.size.width = Components.deviceWidth < 375 ? 13 : 15
        imageView.frame.size.height = Components.deviceWidth < 375 ? 13 : 15
        return imageView
    }()
     var fromPlaceIcon: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "fromIcon")
        imageView.frame.size.width = Components.deviceWidth < 375 ? 13 : 15
        imageView.frame.size.height = Components.deviceWidth < 375 ? 13 : 15
        return imageView
    }()
     var grayView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.appColor.cellViewColor
        return view
    }()
     var timeLabel: UILabel = {
        let label = UILabel()
        label.text = "14:40 - 15:00"
        label.font = UIFont.boldSystemFont(ofSize: 14)
        return label
    }()
     var carImage: UIImageView = {
        let imageView = UIImageView()
        return imageView
    }()
     var tableViewCellView: UIView = {
        let view = UIView()
        view.clipsToBounds = true
        return view
    }()
     var dairyText: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: Components.deviceWidth < 375 ? 21 : 24)
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: "cell")
        print(Components.deviceWidth)
        if Components.deviceWidth < 375 {
            belowIphoneVersion8 = true
        }
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupUI() {
        addSubview(tableViewCellView)
        tableViewCellView.addSubview(fromPlaceName)
        tableViewCellView.addSubview(intoplaceName)
        tableViewCellView.addSubview(fromPlaceIcon)
        tableViewCellView.addSubview(intoplaceicon)
        tableViewCellView.addSubview(grayView)
        tableViewCellView.addSubview(carImage)
        tableViewCellView.addSubview(timeLabel)
        tableViewCellView.addSubview(dairyText)
        // Constraints
        
        fromPlaceIcon.snp.makeConstraints { make in
            make.left.equalTo(tableViewCellView.snp.left).inset(15)
            make.top.equalTo(15)
            make.width.height.equalTo(14)
        }
        
        intoplaceicon.snp.makeConstraints { make in
            make.top.equalTo(fromPlaceIcon.snp.bottom).offset(belowIphoneVersion8 ? 10 : 12)
            make.left.equalTo(tableViewCellView.snp.left).inset(15)
            make.width.height.equalTo(13)
        }
        
        fromPlaceName.snp.makeConstraints { make in
            make.top.equalTo(tableViewCellView.snp.top).inset(14)
            make.leading.equalTo(fromPlaceIcon.snp.trailing).offset(belowIphoneVersion8 ? 8 : 12)
            make.width.equalToSuperview().multipliedBy(0.7)
            make.height.equalTo(14)
        }
        
        intoplaceName.snp.makeConstraints { make in
            make.top.equalTo(fromPlaceName.snp.bottom).offset(belowIphoneVersion8 ? 9 : 11)
            make.leading.equalTo(intoplaceicon.snp.trailing).offset(12)
            make.width.equalToSuperview().multipliedBy(0.7)
            make.height.equalTo(14)
        }
        
        grayView.snp.makeConstraints { make in
            make.bottom.equalTo(tableViewCellView.snp.bottom)
            make.left.right.equalTo(tableViewCellView)
            make.height.equalTo(belowIphoneVersion8 ? 40 : 42)
        }
        
        timeLabel.snp.makeConstraints { make in
            make.left.equalTo(grayView.snp.left).inset(20)
            make.top.equalTo(grayView.snp.top).inset(belowIphoneVersion8 ? 11 : 12)
            make.width.equalTo(200)
        }
        
        carImage.snp.makeConstraints { make in
            make.bottom.equalTo(tableViewCellView.snp.bottom).inset(belowIphoneVersion8 ? 10 : 15)
            make.trailing.equalTo(tableViewCellView.snp.trailing).inset(belowIphoneVersion8 ? 15 : 20)
            make.width.equalTo(belowIphoneVersion8 ? 100 : 108)
            make.height.equalTo(belowIphoneVersion8 ? 35 : 39)
        }
      
        dairyText.snp.makeConstraints { make in
            make.left.equalTo(16)
            make.top.equalTo(tableViewCellView.snp.top).inset(10)
        }
    }
}
