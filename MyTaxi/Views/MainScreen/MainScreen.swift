//
//  ViewController.swift
//  MyTaxi
//
//  Created by mac on 18/12/21.
//

import UIKit
import CoreLocation
import GoogleMaps
import MapKit
import SideMenu
import Lottie

class MainScreen: UIViewController, CLLocationManagerDelegate {
    // MARK: UI Elements
    
    private lazy var menuButton: HighlightedButton = {
        let button = HighlightedButton()
        button.setImage(UIImage(named: "menuIcon"), for: .normal)
        button.backgroundColor = .white
        button.layer.shadowColor = UIColor.black.withAlphaComponent(0.3).cgColor
        button.layer.shadowOpacity = 0.8
        button.layer.shadowOffset = CGSize(width: 3, height: 4)
        button.layer.shadowRadius = 3
        button.addTarget(self, action:  #selector(menuButtonTapped), for: .touchUpInside)
        button.layer.cornerRadius = 20
        return button
    }()
    private lazy var markerImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "mapMarker")
        return imageView
    }()
    private lazy var bottomView: UIView = {
        let view =  UIView()
        view.backgroundColor = .white
        return view
    }()
    private lazy var fromButton: HighlightedButton = {
        let btn = HighlightedButton(type: .system)
        btn.layer.cornerRadius = 15
        btn.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        btn.addTarget(self, action: #selector(fromButtonTapped), for: .touchUpInside)
        btn.layer.shadowColor = UIColor.black.withAlphaComponent(0.3).cgColor
        btn.layer.shadowOpacity = 0.8
        btn.layer.shadowOffset = CGSize(width: 1, height: 2)
        btn.backgroundColor = UIColor.appColor.fromButtonBack
        return btn
    }()
    private lazy var intoButton: HighlightedButton2 = {
        let btn = HighlightedButton2()
        btn.layer.cornerRadius = 15
        btn.layer.shadowColor = UIColor.black.withAlphaComponent(0.3).cgColor
        btn.layer.shadowOpacity = 0.8
        btn.layer.shadowOffset = CGSize(width: 1, height: 2)
        btn.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
        btn.backgroundColor = UIColor.appColor.cellBackgroundColor
       
        return btn
    }()
    private lazy var fromButtonLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.appColor.fromLabelColor
        label.font = UIFont.boldSystemFont(ofSize: 14)
        return label
    }()
    private lazy var fromButtonIcon: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "redPin")
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    private lazy var intoButtonLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.text = "Куда?"
        return label
    }()
    private lazy var intoButtonIcon: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "intoIcon")
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    private lazy var fromMapbutton: UIButton = {
        let btn = UIButton()
        btn.setImage(UIImage(named: "chevronRight"), for: .normal)
        btn.backgroundColor = UIColor.appColor.highlightColor
        btn.layer.cornerRadius = 15
        btn.layer.maskedCorners = [.layerMaxXMaxYCorner]
        return btn
    }()
    private lazy var toMyPlaceButton: HighlightedButton = {
        let button = HighlightedButton()
        button.setImage(UIImage(named: "myPlaceIcon"), for: .normal)
        button.layer.cornerRadius = 20
        button.layer.shadowColor = UIColor.black.withAlphaComponent(0.5).cgColor
        button.layer.shadowOpacity = 0.8
        button.layer.shadowOffset = CGSize(width: 1, height: 2)
        button.backgroundColor = .white
        button.addTarget(self, action: #selector(myPlaceButtonIsTapped), for: .touchUpInside)
        return button
    }()
    private lazy var mapView = GMSMapView()
    
    
    // MARK: Propperties
    private lazy var marker = GMSMarker()
    var menu = SideMenuNavigationController(rootViewController: SideMenu())
    let locationManager = CLLocationManager()
    var currentLocation: CLLocation!
    var coordinate = CLLocationCoordinate2D()
    var isGetlocation = false {
        didSet {
            if isGetlocation {
                print("long = \(coordinate.longitude), lat = \(coordinate.latitude)")
                let camera = GMSCameraPosition.camera(withLatitude: coordinate.latitude, longitude: coordinate.longitude, zoom: 15)
                mapView = GMSMapView(frame: self.view.frame, camera: camera)
                self.view.addSubview(mapView)
                setupUI()
            }
        }
    }
    var lottieView = AnimationView()
    var mapGasture = UIPanGestureRecognizer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.delegate = self
        locationManager.startUpdatingLocation()
        sidebarSettings()
        
        self.lottieView.animation = Animation.named("main_pin")
    }
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.isNavigationBarHidden = true
    }
    
   
    private func setupUI() {
        navigationController?.isNavigationBarHidden = true
        
        mapView.addSubview(bottomView)
        bottomView.snp.makeConstraints { make in
            make.left.right.equalTo(0)
            make.bottom.equalTo(mapView)
            make.height.equalTo(Components.deviceWidth < 375 ? 120 : 137)
        }
        
        mapView.addSubview(menuButton)
        menuButton.snp.makeConstraints { make in
            make.left.equalTo(15)
            make.top.equalTo(Components.deviceWidth > 375 ? 50 : 50)
            make.width.height.equalTo(40)
        }
        
        mapView.addSubview(toMyPlaceButton)
        toMyPlaceButton.snp.makeConstraints { make in
            make.bottom.equalTo(bottomView.snp.top).inset(-20)
            make.right.equalTo(-16)
            make.width.height.equalTo(40)
        }
        
//        view.addSubview(markerImageView)
//        markerImageView.snp.makeConstraints { make in
//            make.centerX.centerY.equalTo(self.view)
//        }
        self.mapView.addSubview(self.lottieView)
        self.lottieView.frame = CGRect(x: Components.deviceWidth / 2, y: Components.deviceHeight/2, width: 20, height: 40)
        self.lottieView.contentMode = .scaleAspectFit
        self.lottieView.translatesAutoresizingMaskIntoConstraints = false
        self.lottieView.snp.makeConstraints { make in
            make.centerX.centerY.equalTo(self.view)
        }
        self.lottieView.play(fromFrame: 0, toFrame: 28, loopMode: .playOnce)

                
        bottomView.addSubview(fromButton)
        fromButton.snp.makeConstraints { make in
            make.top.left.right.equalTo(bottomView).inset(16)
            make.height.equalTo(52)
        }
      
        fromButton.addSubview(fromButtonLabel)
        fromButtonLabel.snp.makeConstraints { make in
            make.top.right.equalTo(fromButton).inset(16)
            make.left.equalTo(fromButton.snp.left).inset(44)
            make.height.equalTo(20)
        }
      
        fromButton.addSubview(fromButtonIcon)
        fromButtonIcon.snp.makeConstraints { make in
            make.top.left.bottom.equalTo(fromButton).inset(16)
        }
        
        bottomView.addSubview(intoButton)
        intoButton.snp.makeConstraints { make in
            make.top.equalTo(fromButton.snp.bottom)
            make.left.equalTo(16)
            make.right.equalTo(-16)
            make.height.equalTo(52)
        }
        
        intoButton.addSubview(intoButtonIcon)
        intoButtonIcon.snp.makeConstraints { make in
            make.top.left.bottom.equalTo(intoButton).inset(16)
        }
        
        intoButton.addSubview(intoButtonLabel)
        intoButtonLabel.snp.makeConstraints { make in
            make.top.right.equalTo(intoButton).inset(16)
            make.left.equalTo(intoButton.snp.left).inset(44)
            make.height.equalTo(20)
        }
        
        intoButton.addSubview(fromMapbutton)
        fromMapbutton.snp.makeConstraints { make in
            make.top.bottom.equalTo(intoButton)
            make.height.equalTo(52)
            make.width.equalTo(60)
            make.right.equalTo(intoButton.snp.right)
        }
    }
    
    
    private func sidebarSettings() {
        menu.menuWidth = Components.deviceWidth - (Components.deviceWidth < 375 ? 70 : 100)
        menu.animationOptions = .transitionCrossDissolve
        SideMenuManager.default.leftMenuNavigationController = menu
        SideMenuManager.default.addPanGestureToPresent(toView: self.view)
        menu.leftSide = true
        view.backgroundColor = .white
        title = ""
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else { return }
        self.coordinate = location.coordinate
        isGetlocation = true
        mapView.delegate = self
    }
    
    @objc private func menuButtonTapped() {
        present(menu, animated: true)
    }
    
    @objc func fromButtonTapped() {
        print("tappppppp")
    }
    
    @objc func myPlaceButtonIsTapped() {
        guard let coordinate = locationManager.location?.coordinate else { return }
        mapView.animate(toZoom: 15)
        let camera = GMSCameraPosition.camera(withLatitude: coordinate.latitude, longitude: coordinate.longitude, zoom: 15)
        mapView.camera = camera
    }
    
    deinit {
        print("MainScreen is deinit")
    }
}

extension MainScreen: GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        self.lottieView.play(fromFrame: 29, toFrame: 109, loopMode: .repeat(40))
    }
    
    func mapView(_ mapView: GMSMapView, idleAt cameraPosition: GMSCameraPosition) {
        let geocoder = GMSGeocoder()
       
        
        geocoder.reverseGeocodeCoordinate(cameraPosition.target) { [weak self] (response, error) in
            guard error == nil, let self = self else { return }
            if let result = response?.firstResult() {
                self.fromButtonLabel.text = result.lines?[0]
                self.marker.title = result.lines?[0]
                self.marker.map = mapView
                self.lottieView.play(fromFrame: 110, toFrame: 130, loopMode: .playOnce)
            }
        }
    }
}
