//
//  MyJourneys.swift
//  MyTaxi
//
//  Created by mac on 20/12/21.
//

import UIKit

class TripHistory: UITableViewController {
    var belowIphoneVersion8 = false
    private lazy var journeysArray: [MyJourneyItemModel] = [
        MyJourneyItemModel(carImage: "", fromPlaceName: "", intoPlaceName: "", time: "6 Июля, Вторник"),
        MyJourneyItemModel(carImage: "car1", fromPlaceName: "улица Sharof Rashidov, Ташкента", intoPlaceName: "5a улица Асадуллы Ходжаева", time: "21:36 - 22:12"),
        MyJourneyItemModel(carImage: "car2", fromPlaceName: "улица Sharof Rashidov, Ташкента", intoPlaceName: "5a улица Асадуллы Ходжаева", time: "14:40 - 15:00"),
        MyJourneyItemModel(carImage: "car3", fromPlaceName: "улица Sharof Rashidov, Ташкента", intoPlaceName: "5a улица Асадуллы Ходжаева", time: "12:00 - 12:19"),
        MyJourneyItemModel(carImage: "", fromPlaceName: "", intoPlaceName: "", time: "5 Июля, Вторник"),
        MyJourneyItemModel(carImage: "car1", fromPlaceName: "улица Sharof Rashidov, Ташкента", intoPlaceName: "5a улица Асадуллы Ходжаева", time: "21:36 - 22:12"),
        MyJourneyItemModel(carImage: "car2", fromPlaceName: "улица Sharof Rashidov, Ташкента", intoPlaceName: "5a улица Асадуллы Ходжаева", time: "14:40 - 15:00"),
        MyJourneyItemModel(carImage: "car3", fromPlaceName: "улица Sharof Rashidov, Ташкента", intoPlaceName: "5a улица Асадуллы Ходжаева", time: "12:00 - 12:19"),
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Мои поездки"
        navigationController?.isNavigationBarHidden = false
        setupUI()
        tableView.register(MyJourneyTableViewCell.self, forCellReuseIdentifier: "journeyCell")
        navigationController?.navigationBar.backIndicatorImage = UIImage(named: "chevronLeft")
        navigationController?.navigationBar.backIndicatorTransitionMaskImage = UIImage(named: "chevronLeft")
        navigationController?.navigationBar.barStyle = .default
        navigationController?.navigationBar.tintColor = .black
    }
    
    override func viewSafeAreaInsetsDidChange() {
        navigationController?.isNavigationBarHidden = false
    }
    private func setupUI() {
        self.tableView.separatorColor = .clear
        if Components.deviceWidth < 375 {
            belowIphoneVersion8 = true
        }
    }
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return journeysArray.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "journeyCell", for: indexPath) as! MyJourneyTableViewCell
        if indexPath.row == 0 || indexPath.row == 4 {
            cell.dairyText.text = journeysArray[indexPath.row].time
            cell.tableViewCellView.frame = CGRect(x: 0, y: 5, width: 250, height: 44)
            cell.grayView.backgroundColor = .clear
            cell.fromPlaceIcon.isHidden = true
            cell.timeLabel.isHidden = true
            cell.intoplaceicon.isHidden = true
        } else {
            cell.fromPlaceName.text = journeysArray[indexPath.row].fromPlaceName
            cell.intoplaceName.text = journeysArray[indexPath.row].intoPlaceName
            cell.carImage.image = UIImage(named: journeysArray[indexPath.row].carImage)
            cell.timeLabel.text = journeysArray[indexPath.row].time
            cell.tableViewCellView.frame = CGRect(x: 16, y: 16, width: Components.deviceWidth - 32, height: belowIphoneVersion8 ? 110 : 118)
            cell.tableViewCellView.layer.borderColor = UIColor.appColor.cellBackgroundColor?.cgColor
            cell.tableViewCellView.layer.borderWidth = 1
            cell.tableViewCellView.layer.cornerRadius = 15
        }
        cell.selectionStyle = .none
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 || indexPath.row == 4 {
            print("tapped data cell")
        }
        else {
            navigationController?.pushViewController(TripDetail(), animated: true)
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 || indexPath.row == 4 {
            return belowIphoneVersion8 ? 40 : 50
        } else {
            return belowIphoneVersion8 ? 122 : 132
        }
    }
    
    deinit {
        print("TripHistory View is deinit")
    }
}
