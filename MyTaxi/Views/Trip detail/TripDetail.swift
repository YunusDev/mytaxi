//
//  TripDetail.swift
//  MyTaxi
//
//  Created by mac on 22/12/21.
//

import UIKit
import CoreLocation
import GoogleMaps
import SnapKit
import Alamofire
import SwiftyJSON

class TripDetail: UIViewController, CLLocationManagerDelegate {
    // MARK: UI Elements
    let scrollView = UIScrollView()
    lazy var mapView = GMSMapView()
    
    private lazy var backButton: UIButton = {
        let btn = UIButton()
        btn.layer.cornerRadius = 18
        btn.setImage(UIImage(named: "chevronLeft"), for: .normal)
        btn.addTarget(self, action: #selector(backButtonTapped), for: .touchUpInside)
        btn.layer.shadowColor = UIColor.black.withAlphaComponent(0.5).cgColor
        btn.layer.shadowOpacity = 0.8
        btn.layer.shadowOffset = CGSize(width: 1, height: 2)
        btn.backgroundColor = .white
        return btn
    }()
    private lazy var mainView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 20
        view.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        view.backgroundColor = .white
        view.frame = CGRect(x: 0, y: Components.deviceWidth - 50, width: Components.deviceWidth, height: 877)
        return view
        
    }()
    private lazy var carNumberView: UIView = {
        let view = UIView()
        view.layer.borderColor = UIColor.appColor.highlightColor?.cgColor
        view.layer.borderWidth = 1
        view.layer.cornerRadius = 8
        view.layer.shadowColor = UIColor.black.withAlphaComponent(0.1).cgColor
        view.layer.shadowOpacity = 0.8
        view.layer.shadowOffset = CGSize(width: 1, height: 2)
        return view
    }()
    private lazy var carRegionNumberLabel: UILabel = {
        let label = UILabel()
        label.text = "25"
        label.font = UIFont.boldSystemFont(ofSize: belowIphoneVersion8 ? 16 : 20)
        return label
    }()
    private lazy var verticalDivider: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.appColor.highlightColor
        return view
    }()
    private lazy var carNumber: UILabel = {
        let label = UILabel()
        label.text = "L 771 FA"
        label.font = UIFont.boldSystemFont(ofSize: belowIphoneVersion8 ? 16 : 20)
        return label
    }()
    private lazy var markOfCarLabel: UILabel = {
        let label = UILabel()
        label.text = "Чёрный Chevrolet Malibu"
        label.font = UIFont.systemFont(ofSize: belowIphoneVersion8 ? 11 : 14, weight: .medium)
        label.textColor = UIColor.appColor.highlightColor
        return label
    }()
    private lazy var divider: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.appColor.dividerColor
        return view
    }()
    private lazy var carImageView: UIImageView = {
        let imageVeiw = UIImageView()
        imageVeiw.image = UIImage(named: "car3")
        return imageVeiw
    }()
    private lazy var fromPlaceName: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.frame.size.width = 200
        label.text = "улица Sharof Rashidov, Ташкент"
        label.frame.size.height = 20
        return label
    }()
    private lazy var intoplaceName: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.text = "5a улица Асадуллы Ходжаева"
        label.frame.size.width = 200
        label.frame.size.height = 20
        return label
    }()
    private lazy var intoplaceicon: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "intoIcon")
        imageView.frame.size.width = belowIphoneVersion8 ? 13 : 15
        imageView.frame.size.height = belowIphoneVersion8 ? 13 : 15
        return imageView
    }()
    private lazy var fromPlaceIcon: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "fromIcon")
        imageView.frame.size.width = belowIphoneVersion8 ? 13 : 15
        imageView.frame.size.height = belowIphoneVersion8 ? 13 : 15
        return imageView
    }()
    private lazy var helpButton: HelpButton = {
        let button = HelpButton()
        button.setImage(UIImage(named: "questionIcon"), for: .normal)
        button.setTitle("Помощь", for: .normal)
        button.titleEdgeInsets = UIEdgeInsets(top: 10, left: -15, bottom: -10, right: 0)
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: Components.deviceWidth / 9, bottom: 20, right: 0)
//        button.adjustImageAndTitleOffsetsForButton(button: button)
        button.setTitleColor(UIColor(named: "helpButtonTitleColor"), for: .normal)
        button.backgroundColor =  UIColor(named: "helpBtnColor")
        button.titleLabel?.font = UIFont.systemFont(ofSize: 12, weight: .semibold)
        button.layer.cornerRadius = 15
        return button
    }()
    private lazy var replayButton: ReplayButton = {
        let button = ReplayButton()
        button.setImage(UIImage(named: "replayButtonicon"), for: .normal)
        button.setTitle("Повторить", for: .normal)
        button.titleEdgeInsets = UIEdgeInsets(top: 10, left: -10, bottom: -10, right: 0)
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left:  Components.deviceWidth / 8, bottom: 20, right: 0)
        button.setTitleColor(UIColor(red: 63/255, green: 123/255, blue: 235/255, alpha: 1), for: .normal)
        button.backgroundColor =  #colorLiteral(red: 0.9254901961, green: 0.9490196078, blue: 0.9921568627, alpha: 1)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 12, weight: .semibold)
        button.layer.cornerRadius = 15
        return button
    }()
    private lazy var callButton: CallButton = {
        let button = CallButton()
        button.setImage(UIImage(named: "callButtonIcon"), for: .normal)
        button.setTitle("Позвонить", for: .normal)
        button.titleEdgeInsets = UIEdgeInsets(top: 10, left: -10, bottom: -10, right: 0)
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: Components.deviceWidth / 9, bottom: 20, right: 0)
        button.setTitleColor(UIColor(red: 71/255, green: 159/255, blue: 106/255, alpha: 1), for: .normal)
        button.backgroundColor =  #colorLiteral(red: 0.9333333333, green: 0.9764705882, blue: 0.9490196078, alpha: 1)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 12, weight: .semibold)
        button.layer.cornerRadius = 15
        return button
    }()
    private lazy var driverLabel: UILabel = {
        let label = UILabel()
        label.text = "Водитель"
        label.font = UIFont.boldSystemFont(ofSize: belowIphoneVersion8 ? 20 : 24)
        return label
    }()
    private lazy var driverDivider: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.appColor.dividerColor
        return view
    }()
    private lazy var driverImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "myPhoto")
        imageView.contentMode = .scaleToFill
        imageView.layer.masksToBounds = true
        imageView.layer.cornerRadius = 25
        return imageView
    }()
    private lazy var driverName: UILabel = {
        let label = UILabel()
        label.text = "Ismatillo Marufkhonov"
        label.minimumScaleFactor = 0.7
        label.font = UIFont.boldSystemFont(ofSize: belowIphoneVersion8 ? 16 : 18)
        return label
    }()
    private lazy var driverRaiting: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14, weight: .semibold)
        label.textColor = UIColor.appColor.dividerColor
        label.text = "Рейтинг: "
        return label
    }()
    private lazy var driverRaitingText: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14, weight: .semibold)
        label.text = "5 ⭐️"
        return label
    }()
    private lazy var travelRaiting: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14, weight: .semibold)
        label.textColor = UIColor.appColor.dividerColor
        label.text = "Поездки: "
        return label
    }()
    private lazy var travelRaitingText: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14, weight: .semibold)
        label.text = "1 885"
        return label
    }()
    private lazy var generalDatalabel: UILabel = {
        let label = UILabel()
        label.text = "Общие данные"
        label.font = UIFont.boldSystemFont(ofSize: belowIphoneVersion8 ? 20 : 24)
        return label
    }()
    private lazy var generalDivider: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.appColor.dividerColor
        return view
    }()
    private lazy var rateLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.appColor.dividerColor
        label.font = UIFont.systemFont(ofSize: 14, weight: .semibold)
        label.text = "Тариф"
        return label
    }()
    private lazy var rateValue: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14, weight: .bold)
        label.text = "Бизнес"
        return label
    }()
    private lazy var paymentMethodlabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.appColor.dividerColor
        label.font = UIFont.systemFont(ofSize: 14, weight: .semibold)
        label.text = "Способ оплаты"
        return label
    }()
    private lazy var paymentMethodValue: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14, weight: .bold)
        label.text = "💶  Наличными"
        return label
    }()
    private lazy var orderLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.appColor.dividerColor
        label.font = UIFont.systemFont(ofSize: 14, weight: .semibold)
        label.text = "Заказ №"
        return label
    }()
    private lazy var orderValue: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14, weight: .bold)
        label.text = "3917866"
        return label
    }()
    private lazy var dateOfTripLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.appColor.dividerColor
        label.font = UIFont.systemFont(ofSize: 14, weight: .semibold)
        label.text = "Дата и время поездки"
        return label
    }()
    private lazy var dateOfTripeValue: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14, weight: .bold)
        label.text = "29 Август, 19:20"
        return label
    }()
    private lazy var durationOfTrip: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.appColor.dividerColor
        label.font = UIFont.systemFont(ofSize: 14, weight: .semibold)
        label.text = "Продолжительность поездки"
        return label
    }()
    private lazy var durationValue: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14, weight: .bold)
        label.text = "00:45"
        return label
    }()
    private lazy var costCalculation: UILabel = {
        let label = UILabel()
        label.text = "Расчёт стоимости"
        label.font = UIFont.boldSystemFont(ofSize: belowIphoneVersion8 ? 20 : 24)
        return label
    }()
    private lazy var costCalculationDivider: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.appColor.dividerColor
        return view
    }()
    private lazy var minimalCost: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.appColor.dividerColor
        label.font = UIFont.systemFont(ofSize: 14, weight: .semibold)
        label.text = "Минимальная сумма"
        return label
    }()
    private lazy var minimalCostValue: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14, weight: .bold)
        label.text = "10,000 UZS"
        return label
    }()
    private lazy var tripAmountLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.appColor.dividerColor
        label.font = UIFont.systemFont(ofSize: 14, weight: .semibold)
        label.text = "Сумма поездки"
        return label
    }()
    private lazy var tripAmountValue: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14, weight: .bold)
        label.text = "31,645 UZS"
        return label
    }()
    private lazy var waitingPrizeLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.appColor.dividerColor
        label.font = UIFont.systemFont(ofSize: 14, weight: .semibold)
        label.text = "Цена ожидании"
        return label
    }()
    private lazy var waitingPrizeValue: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14, weight: .bold)
        label.text = "0 UZS"
        return label
    }()
    private lazy var totalDivider: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.appColor.dividerColor
        return view
    }()
    private lazy var totalLabel: UILabel = {
        let label = UILabel()
        label.text = "Итого"
        label.font = UIFont.boldSystemFont(ofSize: belowIphoneVersion8 ? 20 : 24)
        return label
    }()
    private lazy var totalValue: UILabel = {
        let label = UILabel()
        label.text = "39,600 UZS"
        label.font = UIFont.boldSystemFont(ofSize: belowIphoneVersion8 ? 20 : 24)
        return label
    }()
    private lazy var pinkView: UIView = {
        let view = UIView()
        view.backgroundColor = #colorLiteral(red: 1, green: 0.9176470588, blue: 0.9137254902, alpha: 0.3563053561)
        view.layer.cornerRadius = 25
        return view
    }()
    private lazy var intoIcon: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "intoIcon")
        return imageView
    }()
    
    let locationManager = CLLocationManager()
    var belowIphoneVersion8 = false
    var coordinate = CLLocationCoordinate2D()
    var isGetlocation = false {
        didSet {
            if isGetlocation {
                print("long = \(coordinate.longitude), lat = \(coordinate.latitude)")
                let camera = GMSCameraPosition.camera(withLatitude: coordinate.latitude, longitude: coordinate.longitude, zoom: 10.5)
                mapView = GMSMapView(frame: CGRect(x: 0, y: 0, width: Components.deviceWidth, height: Components.deviceHeight / 2 + 100), camera: camera)
                view.addSubview(mapView)
                setUpPolyline(coordinate: coordinate)
                setupUI()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if Components.deviceWidth < 375 {
            belowIphoneVersion8 = true
        }
        locationManager.delegate = self
        self.locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
      
    }

    override func viewDidLayoutSubviews() {
        scrollView.frame = view.frame
        scrollView.contentInsetAdjustmentBehavior = .never
        scrollView.contentSize = CGSize(width: view.frame.size.width, height: belowIphoneVersion8 ? 1030 : 1200)

    }

    private func setupUI() {
        navigationController?.isNavigationBarHidden = true
        self.view.addSubview(scrollView)
        mapView.addSubview(intoIcon)
        intoIcon.snp.makeConstraints { make in
            make.centerX.centerY.centerY.equalTo(mapView)
            make.width.height.equalTo(0)
        }
        
        view.addSubview(backButton)
        backButton.snp.makeConstraints { make in
            make.left.equalTo(16)
            make.width.height.equalTo(36)
            make.top.equalTo(Components.deviceWidth < 374 ? 30 : 50)
        }
        scrollView.addSubview(mainView)
        
        scrollView.addSubview(carNumberView)
        carNumberView.snp.makeConstraints { make in
            make.top.left.equalTo(mainView).inset(16)
            make.width.equalTo(belowIphoneVersion8 ? 110 : 120)
            make.height.equalTo(belowIphoneVersion8 ? 30 : 32)
        }
        
        carNumberView.addSubview(carRegionNumberLabel)
        carRegionNumberLabel.snp.makeConstraints { make in
            make.top.equalTo(carNumberView.snp.top).inset(5)
            make.left.equalTo(carNumberView.snp.left).inset(6)
            make.height.equalTo(23)
        }
        
        carNumberView.addSubview(verticalDivider)
        verticalDivider.snp.makeConstraints { make in
            make.left.equalTo(carRegionNumberLabel.snp.right).inset(-4)
            make.top.equalTo(carNumberView.snp.top).inset(8)
            make.height.equalTo(16)
            make.width.equalTo(1)
        }
        
        carNumberView.addSubview(carNumber)
        carNumber.snp.makeConstraints { make in
            make.left.equalTo(verticalDivider.snp.right).inset(-4)
            make.top.equalTo(carNumberView.snp.top).inset(5)
        }
        
        mainView.addSubview(markOfCarLabel)
        markOfCarLabel.snp.makeConstraints { make in
            make.top.equalTo(carNumberView.snp.bottom).inset(-8)
            make.left.equalTo(17)
        }
        
        mainView.addSubview(divider)
        divider.snp.makeConstraints { make in
            make.left.equalTo(16)
            make.top.equalTo(markOfCarLabel.snp.bottom).inset(-5)
            make.right.equalTo(-16)
            make.height.equalTo(1)
        }
        
        mainView.addSubview(carImageView)
        carImageView.snp.makeConstraints { make in
            make.top.equalTo(mainView.snp.top).inset(belowIphoneVersion8 ? 22 : 26)
            make.right.equalTo(mainView.snp.right).inset(16)
            make.width.equalTo(belowIphoneVersion8 ? 90 : 106)
        }
        
        mainView.addSubview(fromPlaceIcon)
        fromPlaceIcon.snp.makeConstraints { make in
            make.top.equalTo(divider.snp.bottom).inset(-21)
            make.left.equalTo(21)
            make.width.height.equalTo(14)
        }
        
        mainView.addSubview(intoplaceicon)
        intoplaceicon.snp.makeConstraints { make in
            make.top.equalTo(fromPlaceIcon.snp.bottom).inset(-16)
            make.left.equalTo(21)
            make.width.height.equalTo(13)
        }
        
        mainView.addSubview(fromPlaceName)
        fromPlaceName.snp.makeConstraints { make in
            make.top.equalTo(divider.snp.bottom).inset(-18)
            make.leading.equalTo(fromPlaceIcon.snp.trailing).offset(belowIphoneVersion8 ? 8 : 12)
            make.width.equalToSuperview().multipliedBy(0.7)
            make.height.equalTo(14)
        }
        
        mainView.addSubview(intoplaceName)
        intoplaceName.snp.makeConstraints { make in
            make.top.equalTo(fromPlaceName.snp.bottom).inset(-16)
            make.leading.equalTo(intoplaceicon.snp.trailing).offset(12)
            make.width.equalToSuperview().multipliedBy(0.7)
            make.height.equalTo(14)
        }
        
        mainView.addSubview(helpButton)
        helpButton.snp.makeConstraints { make in
            make.left.equalTo(16)
            make.top.equalTo(intoplaceicon.snp.bottom).inset(belowIphoneVersion8 ? -18 : -21)
            make.height.equalTo(belowIphoneVersion8 ? 50 : 56)
            make.width.equalToSuperview().multipliedBy(0.29)
        }
        
        mainView.addSubview(replayButton)
        replayButton.snp.makeConstraints { make in
            make.left.equalTo(helpButton.snp.right).inset(-8)
            make.top.equalTo(intoplaceicon.snp.bottom).inset(belowIphoneVersion8 ? -18 : -21)
            make.height.equalTo(belowIphoneVersion8 ? 50 : 56)
        }
        
        mainView.addSubview(callButton)
        callButton.snp.makeConstraints { make in
            make.left.equalTo(replayButton.snp.right).inset(-8)
            make.top.equalTo(intoplaceicon.snp.bottom).inset(belowIphoneVersion8 ? -18 : -21)
            make.height.equalTo(belowIphoneVersion8 ? 50 : 56)
            make.right.equalTo(-16)
            make.width.equalToSuperview().multipliedBy(0.29)
        }
        
        mainView.addSubview(driverLabel)
        driverLabel.snp.makeConstraints { make in
            make.top.equalTo(helpButton.snp.bottom).inset(belowIphoneVersion8 ? -20 : -24)
            make.left.equalTo(16)
        }
        
        mainView.addSubview(driverDivider)
        driverDivider.snp.makeConstraints { make in
            make.top.equalTo(driverLabel.snp.bottom).inset(-8)
            make.left.equalTo(16)
            make.right.equalTo(-16)
            make.height.equalTo(1)
        }
        
        mainView.addSubview(driverImageView)
        driverImageView.snp.makeConstraints { make in
            make.top.equalTo(driverDivider.snp.bottom).inset(-12)
            make.left.equalTo(16)
            make.width.height.equalTo(belowIphoneVersion8 ? 50 : 56)
        }
        
        mainView.addSubview(driverName)
        driverName.snp.makeConstraints { make in
            make.top.equalTo(driverDivider.snp.bottom).inset(belowIphoneVersion8 ? -15 : -17)
            make.left.equalTo(driverImageView.snp.right).inset(-16)
            
        }
        
        mainView.addSubview(driverRaiting)
        driverRaiting.snp.makeConstraints { make in
            make.top.equalTo(driverName.snp.bottom).inset(-5)
            make.left.equalTo(driverImageView.snp.right).inset(-16)
        }
        
        mainView.addSubview(driverRaitingText)
        driverRaitingText.snp.makeConstraints { make in
            make.top.equalTo(driverName.snp.bottom).inset(-5)
            make.left.equalTo(driverRaiting.snp.right)
        }
        
        mainView.addSubview(travelRaiting)
        travelRaiting.snp.makeConstraints { make in
            make.top.equalTo(driverName.snp.bottom).inset(-5)
            make.left.equalTo(driverRaitingText.snp.right).inset(-13)
        }
        
        mainView.addSubview(travelRaitingText)
        travelRaitingText.snp.makeConstraints { make in
            make.top.equalTo(driverName.snp.bottom).inset(-5)
            make.left.equalTo(travelRaiting.snp.right)
        }
        
        mainView.addSubview(generalDatalabel)
        generalDatalabel.snp.makeConstraints { make in
            make.top.equalTo(driverImageView.snp.bottom).inset(belowIphoneVersion8 ? -20 : -24)
            make.left.equalTo(16)
        }
        
        mainView.addSubview(generalDivider)
        generalDivider.snp.makeConstraints { make in
            make.top.equalTo(generalDatalabel.snp.bottom).inset(-8)
            make.left.equalTo(16)
            make.right.equalTo(-16)
            make.height.equalTo(1)
        }
        
        mainView.addSubview(rateLabel)
        rateLabel.snp.makeConstraints { make in
            make.top.equalTo(generalDivider.snp.bottom).inset(-12)
            make.left.equalTo(16)
        }
        
        mainView.addSubview(rateValue)
        rateValue.snp.makeConstraints { make in
            make.top.equalTo(generalDivider.snp.bottom).inset(-12)
            make.right.equalTo(-16)
        }
        
        mainView.addSubview(paymentMethodlabel)
        paymentMethodlabel.snp.makeConstraints { make in
            make.top.equalTo(rateLabel.snp.bottom).inset(-15)
            make.left.equalTo(16)
        }
        
        mainView.addSubview(paymentMethodValue)
        paymentMethodValue.snp.makeConstraints { make in
            make.top.equalTo(rateValue.snp.bottom).inset(-15)
            make.right.equalTo(-16)
        }
        
        mainView.addSubview(orderLabel)
        orderLabel.snp.makeConstraints { make in
            make.top.equalTo(paymentMethodlabel.snp.bottom).inset(-12)
            make.left.equalTo(16)
        }
        
        mainView.addSubview(orderValue)
        orderValue.snp.makeConstraints { make in
            make.top.equalTo(paymentMethodValue.snp.bottom).inset(-12)
            make.right.equalTo(-16)
        }
        
        mainView.addSubview(dateOfTripLabel)
        dateOfTripLabel.snp.makeConstraints { make in
            make.top.equalTo(orderLabel.snp.bottom).inset(-12)
            make.left.equalTo(16)
        }
        
        mainView.addSubview(dateOfTripeValue)
        dateOfTripeValue.snp.makeConstraints { make in
            make.top.equalTo(orderValue.snp.bottom).inset(-12)
            make.right.equalTo(-16)
        }
        
        mainView.addSubview(durationOfTrip)
        durationOfTrip.snp.makeConstraints { make in
            make.top.equalTo(dateOfTripLabel.snp.bottom).inset(-12)
            make.left.equalTo(16)
        }
        
        mainView.addSubview(durationValue)
        durationValue.snp.makeConstraints { make in
            make.top.equalTo(dateOfTripeValue.snp.bottom).inset(-12)
            make.right.equalTo(-16)
        }
        
        mainView.addSubview(costCalculation)
        costCalculation.snp.makeConstraints { make in
            make.top.equalTo(durationValue.snp.bottom).inset(belowIphoneVersion8 ? -20 : -24)
            make.left.equalTo(16)
        }
        
        mainView.addSubview(costCalculationDivider)
        costCalculationDivider.snp.makeConstraints { make in
            make.top.equalTo(costCalculation.snp.bottom).inset(-8)
            make.left.equalTo(16)
            make.right.equalTo(-16)
            make.height.equalTo(1)
        }
        
        mainView.addSubview(minimalCost)
        minimalCost.snp.makeConstraints { make in
            make.top.equalTo(costCalculationDivider.snp.bottom).inset(-12)
            make.left.equalTo(16)
        }
        
        mainView.addSubview(minimalCostValue)
        minimalCostValue.snp.makeConstraints { make in
            make.top.equalTo(costCalculationDivider.snp.bottom).inset(-12)
            make.right.equalTo(-16)
        }
        
        mainView.addSubview(tripAmountLabel)
        tripAmountLabel.snp.makeConstraints { make in
            make.top.equalTo(minimalCost.snp.bottom).inset(-12)
            make.left.equalTo(16)
        }
        
        mainView.addSubview(tripAmountValue)
        tripAmountValue.snp.makeConstraints { make in
            make.top.equalTo(minimalCostValue.snp.bottom).inset(-12)
            make.right.equalTo(-16)
        }
        
        mainView.addSubview(waitingPrizeLabel)
        waitingPrizeLabel.snp.makeConstraints { make in
            make.top.equalTo(tripAmountLabel.snp.bottom).inset(-12)
            make.left.equalTo(16)
        }

        mainView.addSubview(waitingPrizeValue)
        waitingPrizeValue.snp.makeConstraints { make in
            make.top.equalTo(tripAmountValue.snp.bottom).inset(-12)
            make.right.equalTo(-16)
        }
        
        mainView.addSubview(totalDivider)
        totalDivider.snp.makeConstraints { make in
            make.top.equalTo(waitingPrizeLabel.snp.bottom).inset(-16)
            make.left.equalTo(16)
            make.right.equalTo(-16)
            make.height.equalTo(1)
        }
        
        mainView.addSubview(totalLabel)
        totalLabel.snp.makeConstraints { make in
            make.top.equalTo(totalDivider.snp.bottom).inset(-12)
            make.left.equalTo(16)
        }
        
        mainView.addSubview(totalValue)
        totalValue.snp.makeConstraints { make in
            make.top.equalTo(totalDivider.snp.bottom).inset(-12)
            make.right.equalTo(-16)
        }
        
        mainView.addSubview(pinkView)
        pinkView.snp.makeConstraints { make in
            make.top.equalTo(totalLabel.snp.bottom).inset(belowIphoneVersion8 ? -22 : -28)
            make.left.equalTo(16)
            make.right.equalTo(-16)
            make.height.equalTo(6)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else { return }
        coordinate = location.coordinate
        isGetlocation = true
    }
    
    private func setUpPolyline(coordinate: CLLocationCoordinate2D) {
        
        // MARK: Define the source latitude and longitude
        let sourceLat = coordinate.latitude
        let sourceLng = coordinate.longitude
            
        // MARK: Define the destination latitude and longitude
        let destinationLat = 41.34075
        let destinationLng = 69.28705
        
        // MARK: Create source location and destination location so that you can pass it to the URL
        let sourceLocation = "\(sourceLat),\(sourceLng)"
        let destinationLocation = "\(destinationLat),\(destinationLng)"
                
        // MARK: Create URL
        let url = "https://maps.googleapis.com/maps/api/directions/json?origin=\(sourceLocation)&destination=\(destinationLocation)&mode=driving&key=AIzaSyA4p9U6LanshNeKDvQcu2itMJQCcwMJ7MU"
        print("xr = \(url)")
        
        AF.request(url).responseJSON { response in
            guard let data = response.data else { return }
            do {
                let json = try JSON(data: data)
                let routes = json["routes"].arrayValue

                for route in routes
                {
                    let routeOverviewPolyline = route["overview_polyline"].dictionary
                    let points = routeOverviewPolyline?["points"]?.stringValue
                    let path = GMSPath.init(fromEncodedPath: points!)

                    let polyline = GMSPolyline(path: path)
                    polyline.strokeColor = .black
                    polyline.strokeWidth = 15.0
                    polyline.map = self.mapView

                }
            } catch let err as NSError {
                print(err.localizedDescription)
            }
        }
        
        let sourceMarker = GMSMarker()
        sourceMarker.position = CLLocationCoordinate2D(latitude: sourceLat, longitude: sourceLng)
        sourceMarker.title = "Xozirgi Joy"
        sourceMarker.icon = UIImage(named: "fromIcon")
        sourceMarker.map = mapView
        
        let destinationMarker = GMSMarker()
        destinationMarker.position = CLLocationCoordinate2D(latitude: 41.34075, longitude: 69.28705)
        destinationMarker.title = "TATU"
        destinationMarker.icon = UIImage(named: "intoIcon")
        destinationMarker.map = mapView
    }
    
    deinit {
        print("TripDetail View is  deinit")
    }
    
    @objc func backButtonTapped() {
        navigationController?.popViewController(animated: true)
    }
    
}

