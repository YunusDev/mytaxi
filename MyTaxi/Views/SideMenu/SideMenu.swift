//
//  MenuViewController.swift
//  MyTaxitask_Marufkhonov_Ismatilo
//
//  Created by mac on 17/12/21.
//

import UIKit

class SideMenu: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    enum MenuOptions: String, CaseIterable {
        case myJourneys = "Мои поездки"
        case paymentMethod = "Способы оплаты"
        case favouritePlaces = "Избранные адреса"
    }
    
    // MARK: UIElements
    private let tabeleView: UITableView = {
        let tableView = UITableView()
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.backgroundColor = UIColor.appColor.sidebarBackground
        return tableView
    }()
    private lazy var userImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.frame = CGRect(x: belowIphoneVersion8 ? 10 : 15, y: belowIphoneVersion8 ? 5 : 10, width: belowIphoneVersion8 ? 70 : 80, height: belowIphoneVersion8 ? 70 : 80)
        imageView.backgroundColor = UIColor.appColor.userBackgroundColor
        imageView.contentMode = .scaleToFill
        imageView.layer.cornerRadius = belowIphoneVersion8 ? 30 : 40
        imageView.layer.masksToBounds = true
        imageView.image = UIImage(named: "myPhoto")
        return imageView
    }()
    private lazy var userNameLabel: UILabel = {
        let userNameLabel = UILabel()
        userNameLabel.text = "Ismatillo Marufkhonov"
        userNameLabel.numberOfLines = 2
        userNameLabel.font = UIFont.systemFont(ofSize: belowIphoneVersion8 ? 14 : 18, weight: .bold)
        userNameLabel.textColor = .white
        userNameLabel.frame = CGRect(x:belowIphoneVersion8 ? 90 : 110, y: 10, width: belowIphoneVersion8 ? 100 : 150 , height: belowIphoneVersion8 ? 40 : 50)
        userNameLabel.minimumScaleFactor = 0.5
        return userNameLabel
    }()
    private lazy var userPhoneNumberLabel: UILabel = {
        let label = UILabel()
        label.text = "+998900031433"
        label.font = UIFont.systemFont(ofSize: belowIphoneVersion8 ? 11 : 14)
        label.textColor = .gray
        label.frame = CGRect(x: belowIphoneVersion8 ? 90 : 110, y: belowIphoneVersion8 ? 50 : 70, width: 120, height: 20)
        label.minimumScaleFactor = 0.5
        return label
    }()
    private lazy var userView: UIView = {
        let userView = UIView()
        userView.layer.cornerRadius = 12
        userView.backgroundColor = UIColor.appColor.userBackgroundColor
        userView.frame = CGRect(x: belowIphoneVersion8 ? 10 : 16, y: 50, width: belowIphoneVersion8 ? Components.deviceWidth - 120 : Components.deviceWidth - 132, height: belowIphoneVersion8 ? 75 : 100)
        return userView
    }()
    
    // MARK: Propperties
    var belowIphoneVersion8 = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tabeleView.frame = view.frame
    }
    
    private func setupUI() {
        if Components.deviceWidth < 376 {
            belowIphoneVersion8 = true
        }
        view.addSubview(tabeleView)
        tabeleView.delegate = self
        tabeleView.dataSource = self
        tabeleView.isScrollEnabled = false
        navigationController?.isNavigationBarHidden = true
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        MenuOptions.allCases.count + 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 110
        } else {
            return belowIphoneVersion8 ? 50 : 60
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        if indexPath.row == 0 {
            view.addSubview(userView)
            userView.addSubview(userImageView)
            userView.addSubview(userNameLabel)
            userView.addSubview(userPhoneNumberLabel)
            cell.backgroundColor = UIColor.appColor.sidebarBackground
            cell.selectionStyle = .none
            cell.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: .greatestFiniteMagnitude)
       }
        else {
            cell.backgroundColor = UIColor.appColor.sidebarBackground
            cell.imageView?.image = UIImage(named: MenuOptions.allCases[indexPath.row - 1].rawValue )
            cell.textLabel?.textColor = .white
            cell.textLabel?.font = UIFont.systemFont(ofSize: belowIphoneVersion8 ? 14 : 16)
            cell.textLabel?.text = MenuOptions.allCases[indexPath.row - 1].rawValue
        }
       
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        switch indexPath.row {
        case 1: navigationController?.pushViewController(TripHistory(), animated: false)
        default: break
        }
    }
    
    deinit {
       print("Side Menu is deinit")
    }
}

