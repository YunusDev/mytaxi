//
//  MyJourneyItemModel.swift
//  MyTaxi
//
//  Created by mac on 18/12/21.
//

import UIKit

struct MyJourneyItemModel  {
    let carImage: String
    let fromPlaceName: String
    let intoPlaceName:String
    let time: String
}

